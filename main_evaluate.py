from pathlib import Path

from argparse import ArgumentParser
from omegaconf import OmegaConf
from tqdm import tqdm
import torch as tr
import pandas as pd

from lightning_module_enhanced import LME
from ngclib.logger import logger
from ngclib.graph_cfg import NGCNodesImporter
from torch.utils.data import DataLoader
from main_train import ConcatReader, build_model

device = tr.device("cuda" if tr.cuda.is_available() else "cpu")

def l1_split_channels(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    """(B,H,W,C) -> (B,C) l2 metrics with mask applied"""
    assert y.shape == gt.shape and len(y.shape) == 4, y.shape
    mask = gt != 0
    y_masked = y * mask
    res_mse_masked = (y_masked - gt).pow(2).sum(axis=(1, 2)) / mask.sum(axis=(1, 2))
    return res_mse_masked

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--test_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the train & graph config path")
    parser.add_argument("--model_path", type=Path, required=True,
                        help="Path to the model dir. {model_dir}/checkpoints is expected to exists" \
                              "The metrics file will be saved there.")
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    args = parser.parse_args()
    if args.nodes_path is None:
        args.nodes_path = Path(__file__).parent / "nodes"
        logger.warning(f"--nodes_path not provided. Defaulting to '{args.nodes_path}'")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    nodes = NGCNodesImporter(cfg.nodes.names, cfg.nodes.types, cfg.nodes.hyper_parameters,
                             nodes_module_path=args.nodes_path).nodes
    output_nodes = [n for n in nodes if n.name not in cfg.nodes.input_nodes]
    if "experiment_name" not in cfg:
        cfg.experiment_name = args.config_path.stem
        logger.warning(f"Experiment name not provided. Defaulting to '{cfg.experiment_name}'")

    test_reader = ConcatReader(path=args.test_set_path, nodes=nodes, out_nodes=output_nodes)
    test_loader = DataLoader(test_reader, **cfg.data.loader_params, collate_fn=test_reader.collate_fn)

    in_channels, out_channels = len(test_reader.in_nodes), len(test_reader.out_nodes)
    model = LME(build_model(cfg, in_channels, out_channels))
    print(model.summary)

    weights_file: Path = args.model_path / "checkpoints/model_best.ckpt"
    model.load_state_from_path(weights_file)

    res = {}
    for item in tqdm(test_loader):
        x, gt = item["data"], item["labels"]
        with tr.no_grad():
            y = model.forward(x)
        res_metric = l1_split_channels(y, gt)
        for b, name in zip(res_metric, item["name"]):
            res[name] = b.tolist()
    res = pd.DataFrame(res, index=output_nodes).T.sort_index()
    res = res * 1000
    res.loc["average"] = res.apply(lambda x: f"{x.mean():.2f} +/- {x.std():.2f}")
    res.to_csv(args.model_path / "results.csv")

if __name__ == "__main__":
    main()
