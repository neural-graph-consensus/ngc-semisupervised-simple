#!/usr/bin/env python3
from pathlib import Path
from typing import List
import os

from argparse import ArgumentParser, Namespace
from omegaconf import OmegaConf, DictConfig
from tqdm import tqdm
from datetime import datetime
import torch as tr
import numpy as np

from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, CopyBestCheckpoint
from ngclib.logger import logger
from ngclib.models import NGCNode
from ngclib.readers import NGCNpzReader
from ngclib.graph_cfg import NGCNodesImporter
from torch.utils.data import DataLoader, ConcatDataset
from torch import nn, optim
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import ModelCheckpoint

from safeuav import SafeUAV
from pseudolabels import do_pseudolabels
from concat_reader import ConcatReader

device = tr.device("cuda" if tr.cuda.is_available() else "cpu")

def accelerator_params(model: nn.Module, cfg: DictConfig):
    accelerator = "gpu" if tr.cuda.is_available() else "cpu"
    if "accelerator_params" not in cfg.train:
        # single gpu/cpu
        return {"accelerator": accelerator, "devices": [0]}
    cfg_acc = cfg.train.accelerator_params
    if cfg_acc.multi_gpu:
        devices = cfg_acc.get("devices", -1)
        return {"accelerator": "gpu", "devices": devices, "strategy": "ddp_find_unused_parameters_false"}
    return {"accelerator": accelerator, "devices": [0]}

def build_model(cfg: DictConfig, in_channels: int, out_channels: int) -> nn.Module:
    """builds a model from the config file based on type/params"""
    if cfg.model.type == "SafeUAV":
        return SafeUAV(in_channels=in_channels, out_channels=out_channels, **cfg.model.parameters)
    raise KeyError(f"Unknown model type: {cfg.model}")

def loss_fn(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    mask = gt != 0
    y = y * mask
    loss = (y - gt).pow(2).sum() / mask.sum()
    return loss

def do_training(model: LME, cfg: DictConfig, experiment_dir: Path, name: str,
                train_loader: DataLoader, val_loader: DataLoader):
    assert name in ("supervised", "semisupervised")
    supervised_last_ckpt = experiment_dir / name / "checkpoints/model_best.ckpt"
    if supervised_last_ckpt.exists():
        logger.warning(f"Supervised checkpoint: '{supervised_last_ckpt}' exists. Returning early.")
        return
    seed_everything(cfg.seed)
    model.reset_parameters()
    model.criterion_fn = loss_fn
    model.callbacks = [ModelCheckpoint(save_last=True, save_top_k=1, monitor="val_loss"),
                       PlotMetrics(),
                       CopyBestCheckpoint()]
    model.optimizer = optim.AdamW(model.parameters(), lr=0.0001)
    acc_params = accelerator_params(model, cfg)
    pl_logger = CSVLogger(save_dir=experiment_dir, name=name, version="")
    Trainer(logger=pl_logger, **cfg.train.trainer_params, **acc_params).fit(model, train_loader, val_loader)


def get_experiment_dir(args: Namespace, cfg: DictConfig) -> Path:
    """
    Returns the experiment dir. If provided via --experiment_dir it's used as-is, otherwise it's generated based on
    the timestamp.
    """
    
    if args.experiment_dir is not None:
        return args.experiment_dir

    if "experiment_name" not in cfg:
        cfg.experiment_name = args.config_path.stem
        logger.warning(f"Experiment name not provided. Defaulting to '{cfg.experiment_name}'")

    timestamp1 = datetime.strftime(datetime.now(), "%Y%m%d")
    timestamp2 = datetime.strftime(datetime.now(), "%H%M%S")
    experiment_dir: Path = Path(__file__).parent / "experiments" / cfg.experiment_name / timestamp1 / timestamp2
    return experiment_dir

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--train_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the train & graph config path")
    parser.add_argument("--validation_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--semisupervised_set_path", type=lambda p: Path(p).absolute() if p is not None else None,
                        required=True)
    parser.add_argument("--experiment_dir", type=Path, help="Path to the experiments dir. Defaults to 'experiments'")
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    args = parser.parse_args()
    if args.nodes_path is None:
        args.nodes_path = Path(__file__).parent / "nodes"
        logger.warning(f"--nodes_path not provided. Defaulting to '{args.nodes_path}'")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    nodes = NGCNodesImporter(cfg.nodes.names, cfg.nodes.types, cfg.nodes.hyper_parameters,
                             nodes_module_path=args.nodes_path).nodes
    output_nodes = [n for n in nodes if n.name not in cfg.nodes.input_nodes]
    experiment_dir = get_experiment_dir(args, cfg)
    logger.info(f"This experiment dir: '{experiment_dir}'")

    train_reader = ConcatReader(path=args.train_set_path, nodes=nodes, out_nodes=output_nodes)
    val_reader = ConcatReader(path=args.validation_set_path, nodes=nodes, out_nodes=output_nodes)
    collate_fn = train_reader.collate_fn
    val_loader = DataLoader(val_reader, **{**cfg.data.loader_params, "shuffle": False}, collate_fn=collate_fn)
    train_loader = DataLoader(train_reader, **cfg.data.loader_params, collate_fn=collate_fn)

    in_channels, out_channels = len(train_reader.in_nodes), len(train_reader.out_nodes)
    model = LME(build_model(cfg, in_channels, out_channels)).to("cpu")
    print(model.summary)

    # step 1) supervised training
    do_training(model, cfg, experiment_dir, "supervised", train_loader, val_loader)

    # step 2) generate pseudo-labels
    do_pseudolabels(model, cfg, experiment_dir, args.semisupervised_set_path,
                    train_reader.in_nodes, train_reader.out_nodes)

    # step 3) semi-supervised training
    pseudo_reader_it2 = ConcatReader(experiment_dir / "pseudolabels", nodes=nodes, out_nodes=output_nodes)
    semisupervised_reader = ConcatDataset([pseudo_reader_it2, train_reader])
    semisupervised_loader = DataLoader(semisupervised_reader, **cfg.data.loader_params, collate_fn=collate_fn)
    do_training(model, cfg, experiment_dir, "semisupervised", semisupervised_loader, val_loader)

if __name__ == "__main__":
    main()
