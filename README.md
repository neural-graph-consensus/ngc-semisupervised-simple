# Semi-supervised learning - basline

The simplest case of semi-supervivsed learning. We define 4 sets: train set, validation set, semi-supervised set and
test set.

We train our seed model, also called iteration 1, or supervised iteration, on the train set + validation set and
evaluate it on the test set.

Then, using the seed model and the semi-supervised set, we generate pseudo-labels. The semi-supervised set has the
same inputs as the train set, but no actual labels.

Finally, we train, from scratch (or not, but in this repo we do), the same original network on the train set plus
the semi-supervised set (gt input, pseudo labels) + validation set. We evaluate it on the same test set.

Built on top of [ngclib](https://gitlab.com/neural-graph-consensus/ngclib). Assumes tasks are nodes and uses
`NGCNpzReader` for reader and `pseudo_algo` function to generate pseudolabels after the first iteration.
Nothing fancy in these, just uses them for convenience.
