from lightning_module_enhanced import LME
from pathlib import Path
import os
from ngclib.models import NGCNode
import numpy as np
import torch as tr
from omegaconf import DictConfig
from torch.utils.data import DataLoader
from tqdm import tqdm

from concat_reader import ConcatReader

def do_pseudolabels(model: LME, cfg: DictConfig, experiment_dir: Path, semisupervised_set_path: Path,
                    in_nodes: list[NGCNode], out_nodes: list[NGCNode]):
    """pseudolabels for semi-supervised training"""
    supervised_best_ckpt = experiment_dir / "supervised/checkpoints/model_best.ckpt"
    # supervised_best_ckpt = [x for x in supervised_last_ckpt.parent.iterdir() if x.name.startswith("epoch")][0]
    pseudolabels_dir = experiment_dir / "pseudolabels"
    if pseudolabels_dir.exists():
        logger.info(f"Pseudolabels dir: '{pseudolabels_dir}' exists. Returning realy")
        return

    # load best supervised checkpoint
    model.load_state_from_path(supervised_best_ckpt)
    # symlink the semi-supervised input data
    pseudolabels_dir.mkdir(exist_ok=False)
    for node in in_nodes:
        out_path = experiment_dir / "pseudolabels" / node.name
        out_path.mkdir(exist_ok=True)
        for item in (semisupervised_set_path / node.name).iterdir():
            if not (out_path / item.name).exists():
                os.symlink(item, out_path / item.name)
    # create a reader from said input data
    pseudolabels_reader = ConcatReader(experiment_dir / "pseudolabels", nodes=in_nodes, out_nodes=[])
    pseudolabels_loader = DataLoader(pseudolabels_reader, **{**cfg.data.loader_params, "shuffle": False},
                                     collate_fn=pseudolabels_reader.collate_fn)
    # generate pseudo-labels
    for item in tqdm(pseudolabels_loader, desc="pseudolabels"):
        with tr.no_grad():
            x = item["data"]
            y: tr.Tensor = model.forward(x)
            y_np: np.ndarray = y.to("cpu").numpy()
            # store the pseudolabels to disk (app specific stuff here, we know they are concatenated)
            for i, node in enumerate(out_nodes):
                (experiment_dir / "pseudolabels" / node.name).mkdir(exist_ok=True)
                for b in range(len(y)):
                    out_path = experiment_dir / "pseudolabels" / node.name / f"{item['name'][b]}.npz"
                    node_data: tr.Tensor = node.save_to_disk(y_np[b, ..., i])
                    np.savez(out_path, node_data)